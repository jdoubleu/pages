# Codeberg Pages
This repository contains the contents of my codeberg page: [https://jdoubleu.codeberg.page](https://jdoubleu.codeberg.page).

## Resources
- Codeberg Page: https://codeberg.page/
- Documentation: https://docs.codeberg.org/codeberg-pages/

## License
Consult the project's page for license information.